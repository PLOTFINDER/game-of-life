#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Tile.hpp"
#include "Board.hpp"
#include "Math.hpp"

Board::Board(Vector p_windowSize,  SDL_Texture* p_texture)
:size(Vector(10,10)), tile_size(Vector(20,20)), tiles({}), texture(p_texture)
{

	for (int x = 0; x < size.x; ++x)
	{
		for (int y = 0; y < size.y; ++y)
		{
			std::cout << "here !!" << std::endl;
			tiles.push_back(Tile(x*tile_size.x,y*tile_size.y, 0));
		}
	}

}

Board::Board(int p_w, int p_h, Vector p_windowSize, SDL_Texture* p_texture)
:size(Vector(p_w,p_h)), tile_size(Vector(20,20)), tiles({}),  texture(p_texture)
{
	for (int x = 0; x < size.x; ++x)
	{
		for (int y = 0; y < size.y; ++y)
		{
			// std::cout << "here !!" << std::endl;
			tiles.push_back(Tile(x*tile_size.x,y*tile_size.y, 0));
		}
	}

	
}

SDL_Texture* Board::getTexture()
{
	return texture;
}

void Board::setTexture(SDL_Texture* p_texture)
{
	texture = p_texture;
}


Vector Board::getSize()
{
	return size;
}

std::vector<Tile> Board::getTiles()
{
	return tiles;
}

int Board::getId()
{
	return id;
}

Vector Board::getTileSize()
{
	return tile_size;
}

void Board::clearTiles()
{
	for(Tile& tile : tiles)
	{
		tile.setValue(0);
	}
}


void Board::update(std::vector<bool> p_mouse_click,bool p_mouse_released, Uint32 p_frame, bool p_pause)
{
	mouse_left_click = p_mouse_click[0];
	mouse_click_released = p_mouse_released;
	tile_selected();
	
	if(p_frame%30==0 && !p_pause)
	{
		count_neighbours();
		apply_changes();
	}
}

void Board::tile_selected()
{
	for(Tile& tile : tiles)
	{
		if(tile.hover() && mouse_left_click && !tile.click)
		{
			if(tile.getValue() == 0)
			{
				tile.setValue(1);
			}
			else
			{
				tile.setValue(0);
			}
			tile.click = true;
		}
		else if(tile.hover() && mouse_click_released && tile.click)
		{
			tile.click = false;
		}
	}
}

void Board::count_neighbours()
{
	std::vector<Vector> patterns = {Vector(-20,-20),Vector(-20,0),Vector(-20,20),Vector(0,-20),Vector(0,20),Vector(20,-20),Vector(20,0),Vector(20,20)};
	for(Tile& tile : tiles)
	{
		Vector pos = tile.getPos();
		int count = 0;

		for (Vector& pattern : patterns)
		{

			if(tileIsHere(pos.x + pattern.x,pos.y + pattern.y))
			{
				count++;
			}
		}
		
		tile.setNeightbours(count);
	}
}

void Board::apply_changes()
{
	for(Tile& tile : tiles)
	{
		if(tile.getNeightbours() == 3){
			if(tile.getValue() != 1)
			{
				tile.setValue(1);
			}
		}
		else if(tile.getNeightbours() > 3 || tile.getNeightbours() < 2)
		{
			if(tile.getValue() == 1)
			{
				tile.setValue(0);
			}
		}
	}
}

bool Board::tileIsHere(float p_x, float p_y)
{
	// std::cout << "x = "<< p_x << ", y = "<<p_y << std::endl;
	for(Tile& tile : tiles)
	{
		Vector pos = tile.getPos(); 
		if(tile.getValue() == 1){
			if(pos.x == p_x && pos.y == p_y)
			{
				return true;
			}
		}
	}
	return false;
}