#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Math.hpp"
#include "Utils.hpp"
#include "RenderWindow.hpp"
#include "Board.hpp"
#include "Tile.hpp"

int main(int argc, char* args[])
{

	const Vector WINDOW_SIZE = Vector(1280, 720);

	if (SDL_Init(SDL_INIT_VIDEO)>0)
	{
		std::cout << "FAIL ON SDL_Init. SDL_ERROR: " <<SDL_GetError() << std::endl;
	}

	if (!(IMG_Init(IMG_INIT_PNG)))
	{
		std::cout << "FAIL ON img_Init. SDL_ERROR: " <<SDL_GetError() << std::endl;
	}

	RenderWindow window("Game", WINDOW_SIZE.x,WINDOW_SIZE.y);

	SDL_Texture* TEXTURE = window.loadTexture("res/img/texture.png");

	Board board = Board(64,36,WINDOW_SIZE, TEXTURE);

	std::vector<bool> mouse_click = {false,false};
	bool released = true;

	bool gameRunning = true;
	bool vsync_enabled = true;

	SDL_Event event;

	const int FPS = 60;

	bool pause = false;

	const int timeStep = 1000 / FPS;
	Uint32 frameStart;
	int frameTime;

	// const float timeStep = 0.01f;

	// float accumulator = 0.0f;
	// float currentTime = utils::hireTimeInSeconds();

	Uint32 count = 0;

	while(gameRunning)
	{	

		frameStart = SDL_GetTicks();
		
		board.update(mouse_click, released, count, pause);
		window.clear();
		window.renderEverything(board);
		window.display();


		while(SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				gameRunning = false;
			}
			else if(event.type == SDL_MOUSEBUTTONDOWN)
			{
				if(event.button.button == SDL_BUTTON_LEFT)
				{
					mouse_click[0] = true;
				}
				if(event.button.button == SDL_BUTTON_RIGHT)
				{
					mouse_click[1] = true;
					pause = !pause;
				}
				if(event.button.button == SDL_BUTTON_MIDDLE)
				{
					board.clearTiles();
				}
				released = false;
			}
			else if(event.type == SDL_MOUSEBUTTONUP)
			{
				if(event.button.button == SDL_BUTTON_LEFT)
				{
					mouse_click[0] = false;
				}
				if(event.button.button == SDL_BUTTON_RIGHT)
				{
					mouse_click[1] = false;
				}
				released = true;
			}
		}


		


		frameTime = SDL_GetTicks() - frameStart;
		// if(timeStep > frameTime)
		// {
		// 	SDL_Delay(timeStep - frameTime);
		// }
		// std::cout << count << std::endl;
		count++;

	}


	window.cleanUp();
	SDL_Quit();

	return 0;

}

