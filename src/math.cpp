#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Math.hpp"

Vector::Vector()
:x(0.0f), y(0.0f)
{}

Vector::Vector(float p_x, float p_y)
:x(p_x), y(p_y)
{}

float Vector::getX()
{
	return x;
}

float Vector::getY()
{
	return y;
}

void Vector::print()
{
	std::cout <<"x =" << x << ", y =" << y << std::endl;
}