#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Math.hpp"
#include "Tile.hpp"

Tile::Tile(Vector p_pos)
:pos(p_pos)
{}

Tile::Tile(int p_x, int p_y, int p_value)
:pos(Vector(p_x,p_y)), value(p_value)
{}

SDL_Rect Tile::getCurrentFrame()
{
	if(value == 0)
	{
		SDL_Rect dst;
		dst.x = 0;
		dst.y = 0;
		dst.w = 8;
		dst.h = 8;

		return dst;
	}
	else
	{
		SDL_Rect dst;
		dst.x = 8;
		dst.y = 0;
		dst.w = 8;
		dst.h = 8;

		return dst;
	}
}

Vector Tile::getPos()
{
	return pos;
}

int Tile::getX()
{
	return pos.x;
}

int Tile::getY()
{
	return pos.y;
}

int Tile::getNeightbours()
{
	return neightbours;
}

void Tile::setNeightbours(int p_value)
{
	neightbours = p_value;
}

int Tile::getLifeSpan()
{
	return life_span;
}

int Tile::getValue()
{
	return value;
}

void Tile::setValue(int p_value)
{
	value = p_value;
}

bool Tile::hover()
{
	int m_x, m_y;
	SDL_GetMouseState(&m_x, &m_y);

	return pos.x<=m_x && m_x<pos.x+20 && pos.y<=m_y && m_y<pos.y+20;
}