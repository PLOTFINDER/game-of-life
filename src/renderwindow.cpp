#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Tile.hpp"
#include "Board.hpp"
#include "RenderWindow.hpp"

RenderWindow::RenderWindow(const char* p_title, int p_w, int p_h)
	:window(NULL), renderer(NULL)
{
	window = SDL_CreateWindow(p_title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, p_w, p_h, SDL_WINDOW_SHOWN);

	if (window == NULL)
	{
		std::cout << "Window failded to init. Error: " << SDL_GetError() << std::endl;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

SDL_Texture* RenderWindow::loadTexture(const char* p_filePath)
{
	SDL_Texture* texture = NULL;
	texture = IMG_LoadTexture(renderer, p_filePath);

	if (texture == NULL)
	{
		std::cout << "Failed to load texture. Error: " << SDL_GetError() << std::endl;
	}
	return texture;
}

void RenderWindow::cleanUp()
{
	SDL_DestroyWindow(window);
}

void RenderWindow::clear()
{
	SDL_RenderClear(renderer);
}

void RenderWindow::render(Tile& p_tile, SDL_Texture* p_texture)
{
	// SDL_Texture* texture = p_texture;
	// SDL_Rect src = p_tile.getCurrentFrame();

	// SDL_Rect dst;
	// dst.x = p_tile.getPos().x;
	// dst.y = p_tile.getPos().y;
	// dst.w = src.w;
	// dst.h = src.h;

	// SDL_Texture* texture = p_tile.getTexture();


	// SDL_RenderCopy(renderer, texture, &src, &dst);
}

void RenderWindow::renderEverything(Board p_board)
{

	std::vector<Tile> tiles = p_board.getTiles();
	Vector tile_size = p_board.getTileSize();
	SDL_Texture* texture = p_board.getTexture();

	for(Tile& tile : tiles)
	{
		
		if(tile.getValue() != 0)
		{
			SDL_Rect src = tile.getCurrentFrame();
			SDL_Rect dst;
			dst.x = tile.getPos().x;
			dst.y = tile.getPos().y;
			dst.w = tile_size.x;
			dst.h = tile_size.y;
			SDL_RenderCopy(renderer, texture, &src, &dst);
		}
	}
}



void RenderWindow::display()
{
	SDL_RenderPresent(renderer);
}