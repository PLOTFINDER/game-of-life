#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Math.hpp"
#include "Tile.hpp"

class Board
{

public:
	int id;
	Board(Vector p_windowSize, SDL_Texture* p_texture);
	Board(int p_w, int p_h, Vector p_windowSize, SDL_Texture* p_texture);
	void update(std::vector<bool> p_mouse_click, bool p_mouse_released, Uint32 p_frame, bool p_pause);
	std::vector<Tile> getTiles();
	Vector getSize();
	Vector getTileSize();
	int getId();
	SDL_Texture* getTexture();
	void setTexture(SDL_Texture* p_texture);
	void clearTiles();


private:
	SDL_Texture* texture;
	Vector size;
	std::vector<Tile> tiles;
	Vector tile_size;
	void count_neighbours();
	void apply_changes();
	void tile_selected();
	bool tileIsHere(float p_x, float p_y);
	bool mouse_left_click;
	bool mouse_click_released;



	

};