#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

class Vector
{
public:
	float x, y;

	Vector();
	Vector(float p_x, float p_y);
	
	float getX();
	float getY();
	void print();
};