#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "Tile.hpp"
#include "Board.hpp"

class RenderWindow
{
public:
	RenderWindow(const char* p_title, int p_w, int p_h);
	SDL_Texture* loadTexture(const char* p_filePath);
	void cleanUp();
	void clear();
	void render(Tile& p_tile, SDL_Texture* p_texture);
	void renderEverything(Board p_board);
	void display();

private:
	SDL_Window* window;
	SDL_Renderer* renderer;

};