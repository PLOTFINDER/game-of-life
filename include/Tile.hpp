#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>

#include "Math.hpp"

class Tile
{
public:
	Tile(Vector p_pos);
	Tile(int p_x, int p_y, int p_value);

	Vector getPos();
	int getX();
	int getY();
	int getNeightbours();
	void setNeightbours(int p_value);
	int getValue();
	int getLifeSpan();
	SDL_Rect getCurrentFrame();
	bool hover();
	void setValue(int p_value);
	bool click = false;

private:
	Vector pos;

	int neightbours;
	int life_span;
	int value;
	
};